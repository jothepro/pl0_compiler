#include "syntree.h"
#include "symtab.h"
#include "config.h"
#include "contree.h"
#include "exptree.h"
#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int sx_labelcount = 1;

/**
 * Creates a new instance of an syntax node.
 * @param type
 * @param next
 * @param br          pointer to the first instr block of a sx_loop or sx_if_else
 * @param br_else     pointer to the second instr block of a sx_if_else
 * @param coord
 * @param condition
 * @param expression
 * @return pointer to the newly created object.
 */
struct sx_node * sx_new_node(
  enum sx_type type,
  struct sx_node * next,
  struct sx_node * br,
  struct sx_node * br_else,
  struct st_coordinates * coord,
  struct ct_node * condition,
  struct et_node * expression,
  struct ft_node * function
) {
  u_debug("syntree.c: sx_new_node(%s, ...)", sx_type_string[type]);
  struct sx_node * out = malloc(sizeof(struct sx_node));
  out->type = type;
  out->next = next;
  out->br = br;
  out->br_else = br_else;
  out->coord = coord;
  out->condition = condition;
  out->expression = expression;
  out->label = sx_labelcount++;
  out->function = function;
  // print subtree of newly created node if DEBUG and DEBUG_SYNTREE is enabled
  DEBUG ? sx_print_tree(out, 1) : NULL;
  return out;
}

/**
 * prints out the tree structure beginning from start to the commandline
 * @param start starting node
 * @param level intendation level, alias recursion level
 */
void sx_print_tree(struct sx_node * start, int level) {
  struct sx_node * node = start;
  while(node) {
    u_syntree("[%5d] %s", level, node->label, sx_type_string[node->type]);
    switch(node->type) {
      case sx_input:
          u_syntree ("( %d | %d )", level + 1, node->coord->level, node->coord->index);
          break;
      case sx_output:
          et_print_tree(node->expression, level + 2);
          break;
      case sx_func_call:
          u_syntree("-> %s [%5d]", level + 1, node->function->name, node->function->label);
          break;
      case sx_assign:
          u_syntree ("( %d | %d )", level + 1, node->coord->level, node->coord->index);
          et_print_tree(node->expression, level + 2);
          break;
      case sx_loop:
          u_syntree("CONDITION", level + 1);
          ct_print_tree (node->condition, level + 2);
          u_syntree("LOOP CONTENT", level + 1);
          sx_print_tree(node->br, level + 2);
        break;
      case sx_if_else:
          u_syntree("CONDITION", level + 1);
          ct_print_tree (node->condition, level + 2);
          u_syntree("IF", level + 1);
          sx_print_tree(node->br, level + 2);
          u_syntree("ELSE", level + 1);
          sx_print_tree (node->br, level + 2);
        break;
      case sx_debug:
        break;
      default:
      ;
    }
    node = node->next;
  }
}


/**
 * insert node at the end of the nodelist. if start is NULL, next will be returned
 * @param start begin of the current nodelist
 * @param next node that should be appended at the end of the nodelist
 * @return pointer to begin of the new nodelist. if start is NULL, next is returned
 */
struct sx_node * sx_nodelist_next(struct sx_node * start, struct sx_node * next) {
  if(DEBUG) {
    u_debug("syntree.c: sx_nodelist_next(%s, %s)", start ? sx_type_string[start->type] : "NULL", next ? sx_type_string[next->type] : "NULL");
  }
  struct sx_node * node = start;
  if(start) {
    while( node && node->next ) node = node->next;
    node->next = next;
    return start;
  } else return next;
}

void sx_to_wat(struct sx_node * start, int intend) {
  struct sx_node * node = start;
  while(node) {
    switch(node->type) {
      case sx_input:
        u_output("i32.const %d\t\t;;level", intend, node->coord->level);
        u_output("i32.const %d\t\t;;index", intend, node->coord->index);
        u_output("call $read\t\t;;read from commandline", intend);
        u_output("call $store\t\t;;save result of $read to (%d|%d)", intend, node->coord->level, node->coord->index);
        break;
      case sx_output:
        et_to_wat(node->expression, intend);
        u_output("call $write\t\t;;write result to commandline", intend);
        break;
      case sx_func_call:
        u_output("i32.const %d\t\t;;stackframe size", intend, node->function->stackframe_size);
        u_output("i32.const %d\t\t;;level delta", intend, node->coord->level);
        u_output("call $new_stackframe", intend);
        u_output("call $%d\t\t;;%s", intend, node->function->label, node->function->name);
        u_output("call $destroy_stackframe", intend);
        break;
      case sx_assign:
        u_output("i32.const %d\t\t;;level", intend, node->coord->level);
        u_output("i32.const %d\t\t;;number", intend, node->coord->index);
        et_to_wat (node->expression, intend);
        u_output("call $store\t\t;;save expression result to (%d|%d)", intend, node->coord->level, node->coord->index);
        break;
      case sx_loop:
        u_output("block", intend);
        u_output("loop", intend + 1);
        ct_to_wat (node->condition, intend + 2);
        u_output("i32.const 1", intend + 2);
        u_output("i32.xor", intend + 2);
        u_output("br_if 1", intend + 2);
        sx_to_wat (node->br, intend + 2);
        u_output("br 0", intend + 2);
        u_output("end", intend + 1);
        u_output("end", intend);
        break;
      case sx_if_else:
        ct_to_wat (node->condition, intend);
        u_output("if", intend);
        sx_to_wat (node->br, intend + 1);
        u_output("else", intend);
        if(node->br_else != NULL) {
          sx_to_wat (node->br_else, intend + 1);
        }
        u_output("end", intend);
        break;
      case sx_debug:
        u_output("call $debug\t\t\t;;print mem to console", intend);
        break;
      default:
      ;
    }
    node = node->next;
  }

}
