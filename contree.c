#include "contree.h"
#include "utils.h"
#include "exptree.h"
#include <stdlib.h>
#include "config.h"

/**
 * Creates a new instance of a condititon node.
 * @param type
 * @param left
 * @param right
 * @return pointer to the newly created struct.
 */
struct ct_node * ct_new_node(enum ct_type type) {
  u_debug("contree.c: ct_new_node(%s)", ct_type_string[type]);
  struct ct_node * out = malloc(sizeof(struct ct_node));
  out->type = type;
  return out;
}


/**
 * print tree structure to console (works recursively)
 * @param node current node
 * @param level current intendation level
 */
void ct_print_tree(struct ct_node * node, int level) {
    // print node to console
    u_syntree("%s", level, ct_type_string[node->type]);
    // start recursion for sub-expressions
    et_print_tree(node->left, level + 1);
    et_print_tree(node->right, level + 1);
}

void ct_to_wat(struct ct_node * node, int intend) {

  et_to_wat (node->left, intend);
  et_to_wat (node->right, intend);
  switch(node->type) {
    case ct_gt:
      u_output("i32.gt_s", intend);
      break;
    case ct_lt:
      u_output("i32.lt_s", intend);
      break;
    case ct_eq:
      u_output("i32.eq", intend);
      break;
    case ct_neq:
      u_output("i32.ne", intend);
      break;
    case ct_gteq:
      u_output("i32.ge_s", intend);
      break;
    case ct_lteq:
      u_output("i32.le_s", intend);
      break;
    case ct_odd:
      u_output("i32.const 1", intend);
      u_output("i32.and", intend);
      u_output("i32.const 1", intend);
      u_output("i32.eq", intend);
      break;
  }
}
