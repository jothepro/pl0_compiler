#ifndef SYMTAB
#define SYMTAB
#include "syntree.h"
#include "funtree.h"

enum st_type { st_var = 1, st_const = 2, st_proc = 4};

static const char * st_type_string[] = {
  "",
  "st_var",
  "st_const",
  "st_var | st_const",
  "st_proc",
  "st_var | st_proc",
  "st_const | st_proc",
  "st_var | st_const | st_proc"
};

struct st_symtab_entry {
  enum st_type type;
  int index;
  struct ft_node * function_node; // pointer to a function node to be called
};

struct st_coordinates {
  int level;
  int index;
};

void st_init();
int st_insert(char* name, enum st_type type);
int st_lookup(char* name, enum st_type type, struct st_coordinates *coord, struct st_symtab_entry ** symtab_entry);
void st_level_up();
int st_level_down();
void st_free(struct st_symtab_entry* entry);
void st_key_free(char * key);
void st_print();
int st_stackframe_size();

#endif
