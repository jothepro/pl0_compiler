#ifndef CONFIG
#define CONFIG
int DEBUG = 0; // print regular debug messages to output
int DEBUG_SYMTAB = 0; // print symtab content to console
int DEBUG_SYNTREE = 0; // print syntax tree with condition and expression trees
int INFO = 0; // show info messages
static const int SYMTAB_INIT_LEVELS = 10; // amount of levels that are allocated on symtab init

// max length of symtab key -> max ident length
#define MAX_KEY_LENGTH 20

char * OUTPUT = "out.wat";

static const char * INTENDATION = "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t";
static const char * BLANK = "                                                                                        ";


static const char * MEMORY_MANAGEMENT = " \n\
  (import \"console\" \"read\" (func $read (result i32))) \n\
  (import \"console\" \"write\" (func $write (param i32))) \n\
  (memory (export \"mem\") 1) \n\
  (func $init \n\
    (i32.store \n\
      (i32.const 0) \n\
      (i32.const 0) \n\
    ) \n\
  ) \n\
  (func $new_stackframe (param $frame_size i32) (param $level_delta i32) \n\
   ;; -----------calculate TOS -------------- \n\
   (local $TOS i32)\n\
   (set_local $TOS \n\
     (i32.add \n\
        (i32.const 0) ;;TOS \n\
        (i32.load) \n\
        (i32.mul ;;value \n\
          (i32.add \n\
            (get_local $frame_size) ;;new TOS creation \n\
            (i32.const 2) ;;new TOS creation \n\
          ) ;;4   new TOS creation \n\
          (i32.const 4) \n\
        ) \n\
      ) ;;new TOS calculation \n\
   )\n\
   ;; ----------------check if current memory is enough for new tos --------\n\
   (block $memory_grow \n\
     (br_if $memory_grow\n\
       (i32.lt_s \n\
          (current_memory)\n\
          (get_local $TOS)\n\
       )\n\
     )\n\
     (block $errorhandling\n\
       (br_if $errorhandling \n\
         (i32.eq \n\
           (i32.const -1) \n\
           (grow_memory \n\
             (i32.sub\n\
               (get_local $TOS)\n\
               (current_memory)\n\
             )\n\
           )\n\
         )\n\
       )\n\
       unreachable\n\
     )\n\
   )\n\
    ;;-----------------DL creation---------------------------------------- \n\
  (i32.store ;;DL creation \n\
    (i32.add \n\
        (i32.const 0) ;;TOS \n\
        (i32.load) ;;value \n\
        (i32.mul \n\
          (i32.sub \n\
            (i32.add \n\
                (get_local $frame_size) \n\
                (i32.const 2) ;;SL and DL \n\
            ) \n\
            (i32.const 1) \n\
          ) \n\
          (i32.const 4) \n\
        ) \n\
    )     ;;DL creation ;;add current TOS \n\
     (i32.const 0) ;;TOS \n\
     (i32.load) ;;value \n\
  ) \n\
   ;;STEP1 -----------------create SL---------- \n\
    (i32.store ;;SL creation \n\
        (i32.add \n\
            (i32.const 0) ;;TOS \n\
            (i32.load) ;;value \n\
            (i32.mul \n\
              (i32.add \n\
                  (get_local $frame_size) \n\
                  (i32.const 2) ;;SL and DL \n\
              ) \n\
              (i32.const 4) \n\
            ) \n\
        ) \n\
     (i32.const 0)     ;;TOS \n\
     (i32.load)	   ;;value \n\
    ) \n\
   ;;-----------------new TOS----------------- \n\
    (i32.store \n\
      (i32.const 0) ;;TOS \n\
      (get_local $TOS) \n\
     )   ;;store at 0 ;;new TOS creation \n\
    ;;STEP2 --------------create SL---------------- \n\
    (block $block1 \n\
      (loop $loopl ;;----BEGIN loop \n\
        (br_if 1 ;;$block1 \n\
            (i32.le_s \n\
               (get_local $level_delta) ;;level_delta <= 0 (Abbruchbedingung) \n\
               (i32.const 0) \n\
            ) \n\
        ) \n\
        ;;----BEGIN dekrement \n\
        (set_local $level_delta \n\
          (i32.sub \n\
            (get_local $level_delta) \n\
            (i32.const 1) \n\
          ) \n\
        ) \n\
        ;;----END dekrement \n\
        (i32.store \n\
          (i32.const 0)	;;adresse \n\
          (i32.load) ;; 12 \n\
          (i32.const 0) ;; 8 \n\
          (i32.load) \n\
          (i32.load) \n\
          (i32.load) \n\
        ) \n\
        (br 0) \n\
      );;----END loop \n\
    ) \n\
  ) \n\
  (func $destroy_stackframe \n\
    (i32.store \n\
      (i32.const 0) \n\
      ;;BEGIN----Zugriff auf DL \n\
        (i32.sub \n\
          (i32.const 0) \n\
          (i32.load) \n\
          (i32.mul \n\
            (i32.const 1) \n\
            (i32.const 4) \n\
          ) \n\
        ) \n\
      (i32.load) \n\
      ;;END----Zugriff auf DL \n\
    ) \n\
  ) \n\
  (func $var_adress (param $level i32) (param $index i32) (result i32) \n\
    (local $adr i32) \n\
    (set_local $adr \n\
    	(i32.const 0)	;;TOS \n\
    	(i32.load)	;;value \n\
    ) \n\
    (block ;;BEGIN block-------------- \n\
      (loop \n\
        (br_if 1 ;;Abbruchbedingung $level == 0 \n\
          (i32.eqz \n\
            (get_local $level) \n\
          ) \n\
        ) \n\
        (set_local $adr \n\
          (get_local $adr) \n\
          (i32.load) \n\
        ) \n\
        (set_local $level \n\
          (i32.sub \n\
            (get_local $level) \n\
            (i32.const 1) \n\
          ) \n\
        ) \n\
        (br 0) \n\
      ) \n\
    );;END block--------------------- \n\
    (i32.sub \n\
      (get_local $adr) \n\
      (i32.mul \n\
        (i32.add \n\
          (i32.const 2) \n\
          (get_local $index) \n\
        ) \n\
      (i32.const 4) \n\
      ) \n\
    ) \n\
  ) \n\
  (func $store (param $level i32) (param $index i32) (param $value i32) \n\
    (i32.store \n\
      (call $var_adress \n\
        (get_local $level) \n\
        (get_local $index) \n\
      ) \n\
      (get_local $value) \n\
    ) \n\
  ) \n\
  (func $load (param $level i32) (param $index i32) (result i32) \n\
    (call $var_adress \n\
      (get_local $level) \n\
      (get_local $index) \n\
    ) \n\
    (i32.load) \n\
  ) \n\
  (func $mem_output (param $entry i32) (result i32) \n\
    (i32.mul \n\
      (get_local $entry) \n\
      (i32.const 4) \n\
    ) \n\
    i32.load \n\
  ) \n\
  (func $debug \n\
    (local $adr i32) \n\
    i32.const -99999 \n\
    call $write \n\
    i32.const 0 \n\
    i32.load   ;; get tos \n\
    set_local $adr \n\
    (loop \n\
      get_local $adr \n\
      i32.load \n\
      call $write \n\
      get_local $adr \n\
      i32.const -4 \n\
      i32.add \n\
      set_local $adr \n\
      get_local $adr \n\
      i32.const 4 \n\
      i32.add \n\
      br_if 0 \n\
    ) \n\
    i32.const -99999 \n\
    call $write \n\
  ) \n\
;;###############################################################\n";

#endif
