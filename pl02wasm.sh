#!/bin/bash

# directory where script is actually located
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

# variable presets
DEBUG=false

# parse commandline options

for var in "$@"
do
    case $var in
        "--help")
            printf "Usage: pl02wasm.sh [--help] [--output=<filename>] [--wat=<wat_filename>] [--wasm=<wasm_filename>] <inputfile>\n\n"
            printf "  --output=<filename>\tset output filename. default is inputfile-name with .html name extension\n"
            printf "  --wat=<wat_filename>\t set name of wat output file\n"
            printf "  --wasm=<wasm_filename\t set namo of wasm output file\n"
            printf "  <inputfile>\t\tselect input pl/0 file\n"
            printf "\n\nThis script will take a pl/0 script as input and covert it to a human readable .wat file and a executable wasm binary embedded in an html file.\n"
            exit 0
            ;;
        "--output="*)
            outputfile=${var:9}
            ;;
        "--wat="*)
            watfile=${var:6}
            ;;
        "--wasm="*)
            wasmfile=${var:7}
            ;;
        "--debug")
            DEBUG=true
            ;;
        *)
            inputfile=$var
            ;;
    esac
done

# check if all nessecary variables have been set and set defaults, if not.
if [ -z $inputfile ]
then
    printf "No inputfile set.\nFind out how to use script: pl02wasm.sh --help\n"
    exit 0
fi

if [ -z $outputfile ]
then
    outputfile="${inputfile}.html"
fi

if [ -z $watfile ]
then
    watfile="${inputfile}.wat"
fi

if [ -z $wasmfile ]
then
    wasmfile="${inputfile}.wasm"
fi

# Compile pl/0 to wat with pl0 compiler

printf "\n# Compiling pl0 to wat...\n\n"

if [ $DEBUG = true ]
then
    ${parent_path}/pl0 --info --debug --syntree --symtab --output=$watfile $inputfile
else
    ${parent_path}/pl0 --output=$watfile $inputfile
fi

if [ $? = 1 ]
then
    printf "# Compiling pl0 to wat failed!\n\n"
    exit 1
fi

# Translate wat to wasm with wat2wasm

printf "\n# Translating wat to wasm...\n\n"
wat2wasm $watfile -o $wasmfile

if [ $? = 1 ]
then
    printf "# Translating wat to wasm failed!\n\n"
    exit 1
fi

# embed result into html template

printf "\n# Create related html from template...\n\n"

sed -e s/INPUTFILENAME/$inputfile/ -e "s/WASMFILENAME/$wasmfile/" ${parent_path}/console.html.template > $outputfile

exit 0
