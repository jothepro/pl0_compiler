#ifndef EXPTREE
#define EXPTREE
#include <stdio.h>

enum et_type {
  et_add,
  et_subst,
  et_mult,
  et_div,
  et_const,
  et_var,
  et_negate,
};

static const char * et_type_string[] = {
  "et_add",
  "et_subst",
  "et_mult",
  "et_div",
  "et_const",
  "et_var",
  "et_negate",
};
/*
 * data structure to represent the bin-tree of an expression.
 */
struct et_node {
  enum et_type type; // node type. if node is of type leaf, the left and right pointers are not defined
  struct et_node * left; // pointer to left node
  struct et_node * right; // pointer to right node
  int constval; // constant value, if node is of type const
  struct st_coordinates* coord; // coordinates for variable, if node is of type var
};

void et_print_tree(struct et_node * node, int level);
struct et_node * et_new_node(enum et_type type, struct et_node * left, struct et_node * right, int constval, struct st_coordinates * coord);
void et_to_wat(struct et_node * node, int intend);
struct et_node * et_append_to_left_end(struct et_node * start, struct et_node * append);
#endif
