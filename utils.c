#include <stdio.h>
#include <stdarg.h>
#include "config.h"
#include "utils.h"
#include "parser.tab.h"
#include "string.h"

void u_debug(const char *format, ...)
{
  if(DEBUG) {
    va_list args;
    va_start(args, format);
    printf( ANSI_STYLE_BOLD ANSI_COLOR_GREEN "%4d DEBUG: \t" ANSI_RESET ANSI_COLOR_GREEN , lineno );
    vprintf(format, args);
    printf( ANSI_RESET "\n" );
    va_end(args);
  }


}

void u_info(const char *format, ...)
{
  if(INFO) {
    va_list args;
    va_start(args, format);
    printf( ANSI_STYLE_BOLD ANSI_COLOR_YELLOW "%4d INFO: \t" ANSI_RESET ANSI_COLOR_YELLOW, lineno  );
    vprintf(format, args);
    printf( ANSI_RESET "\n" );
    va_end(args);
  }


}

void u_error(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  fprintf(stderr, ANSI_STYLE_BOLD ANSI_COLOR_RED "%4d ERROR: \t" ANSI_RESET ANSI_COLOR_RED, lineno );
  vfprintf(stderr, format, args);
  fprintf(stderr, ANSI_RESET "\n");
  va_end(args);
}

void u_warn(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  printf(ANSI_STYLE_BOLD ANSI_COLOR_RED "%4d WARNING: \t" ANSI_RESET ANSI_COLOR_RED, lineno );
  vprintf(format, args);
  printf(ANSI_RESET "\n");
  va_end(args);
}

void u_symtab(const char *format, ...)
{
  va_list args;
  va_start(args, format);
  printf(ANSI_STYLE_BOLD ANSI_COLOR_CYAN "%4d SYMTAB: \t" ANSI_RESET ANSI_COLOR_CYAN, lineno );
  vprintf(format, args);
  printf(ANSI_RESET "\n");
  va_end(args);
}

void u_syntree(const char *format, int level, ...)
{
  va_list args;
  va_start(args, level);
  printf(ANSI_STYLE_BOLD ANSI_COLOR_BLUE "%4d SYNTREE: \t%.*s" ANSI_RESET ANSI_COLOR_BLUE, lineno, level, INTENDATION );
  vprintf(format, args);
  printf(ANSI_RESET "\n");
  va_end(args);
}

void u_output(const char * format, int intend, ...)
{
  va_list args;
  va_start(args, intend);
  fprintf(yyoutput, "%.*s", intend, INTENDATION);
  vfprintf(yyoutput, format, args);
  fprintf(yyoutput, "\n");
  va_end(args);
}


