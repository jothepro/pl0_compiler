#ifndef UTILS
#define UTILS

// color Code defination for colored printf output
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_STYLE_BOLD	   "\033[1m"
#define ANSI_RESET         "\x1b[0m"
FILE * yyoutput;
int lineno = 1;
// useful print methods
void u_debug(const char *format, ...);
void u_info(const char *format, ...);
void u_error(const char *format, ...);
void u_warn(const char *format, ...);
void u_symtab(const char *format, ...);
void u_syntree(const char *format, int level, ...);
void u_output(const char * format, int intend, ...);

#endif


