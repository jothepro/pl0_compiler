#include "config.h"
#include <stdio.h>
#include "syntree.h"
#include "exptree.h"
#include "contree.h"
#include "symtab.h"
#include "utils.h"
#include "funtree.h"
#include "parser.tab.h"
#include "symtab.c"
#include "utils.c"
#include "scanner.yy.c"
#include "parser.tab.c"
#include "syntree.c"
#include "exptree.c"
#include "contree.c"
#include "funtree.c"



void yyerror(const char* msg) {
  u_error("%s at \n%s", msg, yytext);
}


int main(int argc, char **argv) {
	if(argc == 2) {
		if(strcmp(argv[1], "--help") == 0 ) {
			printf("Usage: pl0 [--help] [--debug] [--info] [--symtab] [--syntree] [--output=<outfile>] <inputfile>\n");
      printf("\n  --debug\t\tenable debug output\n");
      printf("  --info\t\toutput info messages\n");
      printf("  --symtab\t\tprint content of symtab to console\n");
      printf("  --syntree\t\tprint content of syntree to console\n");
      printf("  --output=<outfile>\tset output file. default is \"%s\"\n", OUTPUT);

      printf("\n\n");
			return 0;
		}
	}
	for(int i = 1; i < argc; i++) {
		// the last argument without a minus will be the inputfile name
		if(argv[i][0] != '-') {
			yyin = fopen(argv[i], "r");
      if(yyin == NULL) {
        u_error("Inputfile \"%s\" does not exist!", argv[i]);
        return 0;
      }
      u_info("Input file:  \"%s\"", argv[i]);
		} else {
      if(strcmp(argv[i], "--debug") == 0 ) {
			  DEBUG = 1;
		  } else if(strcmp(argv[i], "--info") == 0 ) {
			  INFO = 1;
		  } else if(strcmp(argv[i], "--symtab") == 0) {
        DEBUG_SYMTAB = 1;
      } else if(strcmp(argv[i], "--syntree") == 0) {
        DEBUG_SYNTREE = 1;
      } else if(strncmp(argv[i], "--output=", 9) == 0) {
        int len = strlen(argv[i]);
        OUTPUT = malloc( (len - 8) * sizeof(char) );
        memcpy(OUTPUT, &argv[i][9], len - 9);
        OUTPUT[len-9] = '\0';
      } else {
        u_warn("Unknown argument \"%s\"", argv[i]);
      }
		}
	}
	st_init();

  yyoutput = fopen(OUTPUT, "w");
  if(yyoutput == NULL) {
    u_error("Probem writing to output-file \"%s\"", OUTPUT);
    return 0;
  }
  u_info("Output file: \"%s\"", OUTPUT);
	if(yyparse() == 0) {
    printf(ANSI_COLOR_GREEN ANSI_STYLE_BOLD "\n\n ### Compilation successfull!\n\n" ANSI_RESET);
    fclose(yyin);
    fclose(yyoutput);
	  return 0;

  } else {
    st_print();
    printf(ANSI_COLOR_RED ANSI_STYLE_BOLD "\n\n ### Compilation failed!\n\n" ANSI_RESET);
    fclose(yyin);
    fclose(yyoutput);
	  return 1;
  }
}

