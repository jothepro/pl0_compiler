#ifndef CONTREE
#define CONTREE
#include "exptree.h"
#include <stdio.h>

enum ct_type {
  ct_gt, // greater than
  ct_lt, // lower than
  ct_eq, // equal
  ct_neq, // not equal
  ct_gteq, // greater than or equal
  ct_lteq, // smaller than or equal
  ct_odd // odd, only has left sibling
};

// string representation of enums for debugging purposes
static const char* ct_type_string[] = {
  "ct_gt",
  "ct_lt",
  "ct_eq",
  "ct_neq",
  "ct_gteq",
  "ct_lteq",
  "ct_odd"
};

struct ct_node {
  enum ct_type type;
  struct et_node * left;
  struct et_node * right;
};

void ct_print_tree(struct ct_node * node, int level);
struct ct_node * ct_new_node(enum ct_type type);
void ct_to_wat(struct ct_node * node, int intend);

#endif
