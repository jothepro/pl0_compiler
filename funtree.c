#include "funtree.h"
#include "syntree.h"
#include "utils.h"
#include "config.h"
#include <stdio.h>
#include <stdlib.h>

void ft_print_tree(struct ft_node * start) {
  struct ft_node * node = start;
  u_debug("funtree.c ft_print_tree(%s)", start?start->name:"NULL");
  while(node) {
    u_syntree ("function %d %s", 0, node->label, node->name);
    sx_print_tree(node->function, 1);
    node = node->next;
  }
}
struct ft_node * ft_new_node(struct sx_node * function, char * name, int label, int stackframe_size) {
  u_debug("funtree.c: ft_new_node( %s, %d )", name, label);
  char * name_cpy = malloc(sizeof(char) * MAX_KEY_LENGTH);
	strncpy(name_cpy, name, MAX_KEY_LENGTH);
  struct ft_node * output = malloc(sizeof(*output));
  output->function = function;
  output->name = name_cpy;
  output->label = label;
  output->stackframe_size = stackframe_size;
  output->next = NULL;
  return output;
}
void ft_to_wat(struct ft_node * start) {
  struct ft_node * node = start;
  if(DEBUG) {
    u_debug("funtree.c: ft_to_wat(%s)", node->name);
  }

  u_output("(module", 0);
  u_output(MEMORY_MANAGEMENT, 0);

  while(node) {
    u_output("(func $%d\t\t\t;;%s", 1, node->label, node->name);
    if(node->label == 0) {
      // if we just entered main, init main stackframe
      u_output("call $init", 2);
      u_output("i32.const %d\t\t;;main stackframe size", 2, node->stackframe_size);
      u_output("i32.const 0\t\t;;level delta of main function", 2);
      u_output("call $new_stackframe", 2);
    }
    sx_to_wat(node->function, 2);
    if(node->label == 0) u_output("call $destroy_stackframe", 2);
    u_output(")", 1);
    node = node->next;
  }

  u_output("(export \"main\" (func $0))", 1);
  u_output(")", 0);
}

struct ft_node * ft_nodelist_next(struct ft_node * start, struct ft_node * next) {
  if(DEBUG) {
    u_debug("funtree.c: ft_nodelist_next(%s, %s)", start?start->name:"NULL", next?next->name:"NULL");
  }
  struct ft_node * node = start;
  if(start) {
    while( node && node->next ) node = node->next;
    node->next = next;
    return start;
  } else return next;
}
