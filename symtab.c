#include "symtab.h"
#include "utils.h"
#include "config.h"
#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int st_levels; // number of levels that are currently allocated
GHashTable* *st_symtab; // the HashTable array
int *st_stackframe_size_count; // array for keeping track of the required stackframe_size
                               // size on each level. if a var or const is
                               // inserted in the hashtable, the st_stackframe_size_count
                               // gets incremented.
int st_level; // current level

/**
 * Initializes the GHashTable. Must be called once before using the SymTab!
 */
void st_init() {
  u_debug("symtab.c: st_init()");
  st_levels = SYMTAB_INIT_LEVELS;
  u_debug("\t> st_levels = %d", st_levels);
  st_level = -1;
  u_debug("\t> initializing GHashTable");
  st_symtab = malloc(st_levels * sizeof(GHashTable *));
  st_stackframe_size_count = malloc(st_levels * sizeof(int));
}

/**
 * Insert a new entry into the Hashmap at the currently active level.
 * @return 0 if insert went well
 * @return 1 if key already existed
 */
int st_insert(char* name, enum st_type type) {
  // name needs to be copied over to a new place.
  char* cp_name = malloc( MAX_KEY_LENGTH * sizeof(char) );
  strncpy(cp_name, name, MAX_KEY_LENGTH - 1);
  cp_name[MAX_KEY_LENGTH - 1] = '\0';
  u_debug("symtab.c: st_insert( %s, %s)", cp_name, st_type_string[type]);
  // check if element does not yet exists in current level
  if( !g_hash_table_contains(st_symtab[st_level], cp_name ) ) {
    // if var or const, increment st_stackframe_size_count
    if( ( st_var | st_const ) & type ) st_stackframe_size_count[st_level]++;
    // get current number of elements in the current level.
    int st_level_size = g_hash_table_size(st_symtab[st_level]);
    // create Entry object
    struct st_symtab_entry* symtab_entry = malloc(sizeof( *symtab_entry));
    symtab_entry->type = type;
    symtab_entry->index = st_level_size;
    // insert Entry into Hashtable
    // g_hash_table_insert returns TRUE if the key did not exist yet
    u_debug("\t> insert key into GHashTable on Level %d", st_level);
    return g_hash_table_insert(st_symtab[st_level], cp_name, symtab_entry );
  } else {
    u_error("Identifier \"%s\" has already been declared in this scope.", cp_name);
    free(cp_name);
    return 0; //  if key already exists
  }
}

/**
 * checks if a key exists.
 * @param name
 * @param type list of types, that we are searching for, | concatenated
 * @param coord
 * @param symtab_entry matching entry
 * @return 0 if key has not been found
 * @return -1 if key exists, but is of wrong type
 * @return 1 if element has been found and matches the type
 */
int st_lookup(char* name, enum st_type type, struct st_coordinates *coord, struct st_symtab_entry ** symtab_entry) {
  u_debug("symtab.c: st_lookup( %s, %s, ..)", name, st_type_string[type]);
  for(int i = 0; i <= st_level; i++) {
    u_debug("\t> in Level %d", st_level - i);
    if( g_hash_table_contains(st_symtab[st_level - i], name) ) {
      struct st_symtab_entry* entry = g_hash_table_lookup(st_symtab[st_level - i], name );
      if( entry->type & type )  {
        if(coord != NULL) {
          coord->level = i;
          coord->index = entry->index;
        }
        symtab_entry != NULL ? *symtab_entry = entry : NULL;
        return 1;
      } else {
        u_error("Identifier %s has been declared, but is of wrong type.", name);
        return -1;
      }
    }
  }
  u_error("Identifier %s has not been declared.", name);
  return 0;
}

/**
 * Jumps one level up.
 */
void st_level_up() {
  u_debug("symtab.c: st_level_up()");
  // check if new space needs to be allocated
  if(!(st_level < st_levels)) {
    int new_size = st_levels * 2;
    u_debug("\t> allocating new space for more levels. New array size: %d", new_size);
    st_symtab = realloc(st_symtab, new_size * sizeof(GHashTable *));
    st_stackframe_size_count = realloc(st_stackframe_size_count, new_size * sizeof(char));
    st_levels = new_size;
  }
  // incrementing current level counter
  st_level++;
  u_debug("\t> creating new GHashTable on Level %d", st_level);
  // init hashtable and stackframe count
  st_symtab[st_level] = g_hash_table_new_full( g_str_hash ,g_str_equal, (void *) st_key_free, (void *) st_free);
  st_stackframe_size_count[st_level] = 0;
}

/**
 * returns the stackframe size that will be required for this level on runtime
 */
int st_stackframe_size() {
  return st_stackframe_size_count[st_level];
}

/**
 * jumps one level down and destroys the table.
 * @return 0 if we are already on level 0
 * @return 1 if level_down worked well
 */
int st_level_down() {
  st_print();
  u_debug("symtab.c: st_level_down()");
  u_debug("          stackframe size: %d", st_stackframe_size_count[st_level]);
  if(st_level >= 0) {
    u_debug("\t> destroying GHashTable at Level %d", st_level);
    g_hash_table_destroy(st_symtab[st_level]);
    st_level--;
    return 1;
  } else
    u_error("Symtab level down failed, already on level 0.");
    return 0;
}

/**
 * Needed for glibc HashTable Library to free the memory when the table is
 * deleted. Simply frees the memory of the entry.
 */
void st_free(struct st_symtab_entry* entry) {
  u_debug("\t> freeing space from token with type %s", st_type_string[entry->type]);
  free(entry);
}

/**
 * Needed for glibc HashTable Library to free the memory  of the key when the
 * table is deleted.
 */
void st_key_free(char * key) {
  u_debug("\t> freeing space for key %s", key);
  free(key);
}

/**
 * prints current symtab enry, if DEBUG_SYMTAB is enabled
 */
void st_print() {
  if(DEBUG_SYMTAB) {
    char * line = malloc(st_levels * ( MAX_KEY_LENGTH + 2) * sizeof(char));
    char * tmp = malloc( MAX_KEY_LENGTH * sizeof(char) + 2 );
    // headline
    line[0] = '\0';
    for(int i = 0; i <= st_level; i++) {
      tmp[0] = '\0';
      snprintf(tmp, MAX_KEY_LENGTH + 2, "%.*sLevel %4d|", MAX_KEY_LENGTH - 10, BLANK, i);
      strncat(line, tmp, MAX_KEY_LENGTH + 2);
    }
    u_symtab(line);

    // divison line
    line[0] = '\0';
    for(int i = 0; i <= st_level; i++) {
      tmp[0] = '\0';
      for(int j = 0; j < MAX_KEY_LENGTH; j++)
        tmp[j] = '-';
      tmp[MAX_KEY_LENGTH ] = '|';
      tmp[MAX_KEY_LENGTH + 1] = '\0';
      strncat(line, tmp, MAX_KEY_LENGTH + 2);
    }
    u_symtab(line);

    // entries
    char *** levels = malloc((st_level + 1) * sizeof(char **));
    int *level_lengths = malloc( (st_level + 1) * sizeof(int) );
    int max_length = 0;
    for(int i = 0; i <= st_level; i++) {
      int level_length;
      levels[i] = (char **)g_hash_table_get_keys_as_array(st_symtab[i], &level_length);
      level_lengths[i] = level_length;
      if(max_length < level_length) {
        max_length = level_length;
      }
    }
    for(int i = 0; i < max_length; i++) {
      line[0] = '\0';
      for(int j = 0; j <= st_level; j++ ) {
        if(i < level_lengths[j]) {
          tmp[0] = '\0';
          snprintf(tmp, MAX_KEY_LENGTH + 2, "%s|", levels[j][i]);
          // fill up the rest with whitespace
          int whitespace = MAX_KEY_LENGTH - strlen(tmp);
          for(int j = 0; j <= whitespace; j++)
            strcat(line, " ");
          strncat(line, tmp, MAX_KEY_LENGTH + 2);
        } else {
          for(int k = 0; k < MAX_KEY_LENGTH; k++)
            strcat(line, " ");
          strcat(line, "|");
        }

      }
      u_symtab(line);
    }
    // free all allocated memory
    free(line);
    free(tmp);
    free(levels);
    free(level_lengths);
  }
}
