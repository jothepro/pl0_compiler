pl0: scanner.yy.c parser.tab.c main.c symtab.c symtab.h utils.c utils.h config.h syntree.c syntree.h exptree.h exptree.c contree.h contree.c funtree.c funtree.h
	# Compiling all sources...
	/usr/bin/gcc -o pl0 -I. `pkg-config --cflags glib-2.0` main.c `pkg-config --libs glib-2.0 --static`

scanner.yy.c: parser.tab.h scanner.l
	# Building scanner...
	flex -oscanner.yy.c scanner.l

parser.tab.c parser.tab.h: parser.y
	# Building parser...
	bison --yacc --verbose --graph --defines=parser.tab.h --output=parser.tab.c parser.y

clean:
	# Removing all generated files...
	rm --force *.yy.c *~ pl0 *.tab.c *.tab.h *.exe *.output *.dot *.png *.html *.wasm *.wat
test: pl0
	# Run test configuration
	./pl0 --info --debug --symtab --syntree --output=test.wat test.pl0

valgrind: pl0
	# Run test configuration under valgrind
	valgrind --leak-check=yes ./pl0 --info --debug --symtab --syntree --output=test.wat test.pl0
	
