#ifndef SYNTREE
#define SYNTREE
#include "symtab.h"
#include "contree.h"
#include "exptree.h"
#include <stdio.h>

enum sx_type {
  sx_input,
  sx_output,
  sx_func_call,
  sx_assign,
  sx_loop,
  sx_if_else,
  sx_debug
};

// string representation of enum for debugging purposes
static const char* sx_type_string[] = {
  "sx_input",
  "sx_output",
  "sx_func_call",
  "sx_assign",
  "sx_loop",
  "sx_if_else",
  "sx_debug"
};


struct sx_node {
  enum sx_type type;              // | Input	| output	| proc_call	| cond_jmp	| jmp	| assign	| nop	|
                                  //----------------------------------------------------------------------------
  struct sx_node * next;          // |	x	|	x		|	x		| 	x		| 	x	|	x		|	x	|
  struct sx_node * br;            // |		|			|			| 	x		| 	x	|			|		|
  struct sx_node * br_else; // jmp pointer for else in if_else node
  struct st_coordinates * coord;  // |	x	|			|	x		|			|		|	x		|		|
  struct ct_node * condition;     // |		|			|			|	x		|		|			|		|
  struct et_node * expression;    // |		|	x		|			|			|		|	x		|
  int label; // label for debugging the jumps
  struct ft_node * function; // pointer to function node to be called
};

void sx_print_tree(struct sx_node * start, int level);
struct sx_node * sx_new_node(
  enum sx_type type,
  struct sx_node * next,
  struct sx_node * br,
  struct sx_node * br_else,
  struct st_coordinates * coord,
  struct ct_node * condition,
  struct et_node * expression,
  struct ft_node * function
);
struct sx_node * sx_nodelist_next(struct sx_node * start, struct sx_node * next);
void sx_to_wat(struct sx_node * start, int intend);

#endif
