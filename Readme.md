# pl0 to wat Compiler
by Johannes Krafft and Anja Dannenberg 2017

## Dependencies
* libc
* glib-2.0
* bison
* flex
* wat2wasm converter: https://github.com/webassembly/wabt
	- You need to build and install this dependency on your own
	- Make sure that the wat2wasm command works in your commandline

## Building
build compiler with
```
make
```
the resulting binary will be called `pl0`


## Using
get full instructions for the compiler usage with `pl0 --help`

the pl0 compiler will compile to WebAssembly Text Format (.wat).

## Executing pl0 program
use the bashscript `pl02wasm.sh` to directly compile to WebAssembly Binary Format.
It will automatically first call the `pl0` compiler and then convert the result to
wasm. The wasm is then embedded in a html file, so if you want to run your program,
all you need to do is to load the html-file with the browser of your trust.
