#include "exptree.h"
#include "config.h"
#include "utils.h"
#include "symtab.h"
#include <stdlib.h>

/**
 * Creates a new instance of an expression node.
 * @param type
 * @param left
 * @param right
 * @param constval
 * @param coord
 * @return pointer to the newly created struct.
 */
struct et_node * et_new_node(enum et_type type, struct et_node * left, struct et_node * right, int constval, struct st_coordinates * coord) {
  u_debug("exptree.c: et_new_node(%s, ...)", et_type_string[type]);
  struct et_node * out = malloc(sizeof(struct et_node));
  out->type = type;
  out->left = left;
  out->right = right;
  out->constval = constval;
  out->coord = coord;
  DEBUG ? et_print_tree (out, 1) : NULL;
  return out;
}

/**
 * print tree structure to console (works recursively)
 * @param node current node
 * @param level current intendation level
 */
void et_print_tree(struct et_node * node, int level) {
  if( node ) {
    // print node to console
    u_syntree("%s", level, et_type_string[node->type]);
    // go into recursion, if we don't face a leaf node, otherwise print leaf content
    switch(node->type) {
      case et_const:
        u_syntree("%d", level, node->constval);
        break;
      case et_var:
        u_syntree("( %d | %d )", level, node->coord->level, node->coord->index);
        break;
      default:
        et_print_tree(node->left, level + 1);
        et_print_tree(node->right, level + 1);
    }
  }
}

void et_to_wat(struct et_node * node, int intend) {
  if(node != NULL) {
    switch(node->type) {
      case et_add:
        et_to_wat (node->left, intend);
        et_to_wat (node->right, intend);
        u_output("i32.add", intend);
        break;
      case et_subst:
        et_to_wat (node->left, intend);
        et_to_wat (node->right, intend);
        u_output("i32.sub", intend);
        break;
      case et_mult:
        et_to_wat (node->left, intend);
        et_to_wat (node->right, intend);
        u_output("i32.mul", intend);
        break;
      case et_div:
        et_to_wat (node->left, intend);
        et_to_wat (node->right, intend);
        u_output("i32.div_s", intend);
        break;
      case et_const:
        u_output("i32.const %d", intend, node->constval);
        break;
      case et_var:
        u_output("i32.const %d\t\t;;level-delta", intend, node->coord->level);
        u_output("i32.const %d\t\t;;index", intend, node->coord->index);
        u_output("call $load\t\t;;load (%d|%d)", intend, node->coord->level, node->coord->index);
        break;
      case et_negate:
        et_to_wat (node->left, intend);
        u_output("i32.const -1", intend);
        u_output("i32.mul\t\t\t;;negate", intend);
        break;

    }
  }
}


struct et_node * et_append_to_left_end(struct et_node * start, struct et_node * append) {
  struct et_node * tmp = start;
  while ( tmp && tmp->left ) tmp = tmp->left;
  if ( tmp ) {
    tmp->left = append;
    return start;
  } else {
    return append;
  }
}


