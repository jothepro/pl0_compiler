#ifndef FUNTREE
#define FUNTREE
#include "symtab.h"
#include "contree.h"
#include "exptree.h"
#include "syntree.h"
#include <stdio.h>

struct ft_node {
  struct sx_node * function;
  struct ft_node * next;
  int stackframe_size;
  int label;
  char * name;
};

void ft_print_tree(struct ft_node * start);
struct ft_node * ft_new_node(struct sx_node * function, char * name, int label, int stackframe_size);
struct ft_node * ft_nodelist_next(struct ft_node * start, struct ft_node * next);
void ft_to_wat(struct ft_node * start);

#endif
