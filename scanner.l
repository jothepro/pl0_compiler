%{
  #include "parser.tab.h"
  #include "utils.h"
  void yyerror(const char* msg);
  int comment = 0;
%}
/* Start Condititons */
%x C_COMMENT
%option yylineno

/* DEFINITIONS */
/*
NULSYM
*/
IDENTSYM    [A-Za-z]{1}[0-9a-zA-Z]{0,19}
NUMBERSYM   [0-9]+
PLUSSYM     "+"
MINUSSYM    "-"
MULTSYM     "*"
SLASHSYM    "/"
ODDSYM      odd|ODD
EQLSYM      "="
NEQSYM      "<>"
LESSYM      "<"
LEQSYM      "<="
GTRSYM      ">"
GEQSYM      ">="
LPARENTSYM  "("
RPARENTSYM  ")"
COMMASYM    ","
SEMICOLONSYM ";"
PERIODSYM   "."
BECOMESSYM  ":="
BEGINSYM    begin|BEGIN
ENDSYM      end|END
IFSYM       if|IF
THENSYM     then|THEN
WHILESYM    while|WHILE
DOSYM       do|DO
CALLSYM     call|CALL
CONSTSYM    const|CONST
INTSYM      int|INT|var|VAR
PROCSYM     procedure|PROCEDURE
OUTSYM      out|OUT|"!"
INSYM       in|IN|"?"
ELSESYM     else|ELSE
DEBUGSYM    debug|DEBUG


%%
"/*"            { BEGIN(C_COMMENT); }
<C_COMMENT>"*/" { BEGIN(INITIAL); }
<C_COMMENT>\n   { lineno++; }
<C_COMMENT>.    { }
{DEBUGSYM}      return DEBUGSYM;
{NUMBERSYM}     { yylval.number = atoi(yytext); return NUMBERSYM; }
{PLUSSYM}       return PLUSSYM;
{MINUSSYM}      return MINUSSYM;
{MULTSYM}       return MULTSYM;
{SLASHSYM}      return SLASHSYM;
{ODDSYM}        return ODDSYM;
{EQLSYM}        return EQLSYM;
{NEQSYM}        return NEQSYM;
{LESSYM}        return LESSYM;
{LEQSYM}        return LEQSYM;
{GTRSYM}        return GTRSYM;
{GEQSYM}        return GEQSYM;
{LPARENTSYM}    return LPARENTSYM;
{RPARENTSYM}    return RPARENTSYM;
{COMMASYM}      return COMMASYM;
{SEMICOLONSYM}  return SEMICOLONSYM;
{PERIODSYM}     return PERIODSYM;
{BECOMESSYM}    return BECOMESSYM;
{BEGINSYM}      return BEGINSYM;
{ENDSYM}        return ENDSYM;
{IFSYM}         return IFSYM;
{THENSYM}       return THENSYM;
{WHILESYM}      return WHILESYM;
{DOSYM}         return DOSYM;
{CALLSYM}       return CALLSYM;
{CONSTSYM}      return CONSTSYM;
{INTSYM}        return INTSYM;
{PROCSYM}       return PROCSYM;
{OUTSYM}        return OUTSYM;
{INSYM}         return INSYM;
{ELSESYM}       return ELSESYM;
{IDENTSYM}      { strncpy(yylval.string, yytext, MAX_KEY_LENGTH); return IDENTSYM; }
\n              lineno++;
[ ]|\t          ;
.               return ERR;
%%
int yywrap() {
    return 1;
}

