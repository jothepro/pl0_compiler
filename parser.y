%{
  #include <stdio.h>
  #include "symtab.h"
  #include "utils.h"
  #include "syntree.h"
  #include "exptree.h"
  #include "contree.h"
  #include "funtree.h"
  int yylex(void);
  void yyerror(const char* msg);
  extern FILE * yyin;
  int token;
  int function_count = 1; // variable for labeling all procedures with a
                          // ongoing number. number 0 is reserved for the
                          // main procedure
  int var_const_count = 0; // temp variable needed to store the required
                          // stackframe size in the function node at
                          // the end of a block. Before each level down a
                          // value is assigned to this variable. When the
                          // block is reduced to a procedure, the stored
                          // value can be written to the function node

  struct ft_node * functions = NULL; // pointer to list of all functions. Newly detected
                              // functions are added to the end of this list

  /**
  * building node structure for declaring const variables.
  * When a const is declared, the assigned value needs to be written to the
  * memory. Has been outsourced from parser, because it is needed twice there
  */
  struct sx_node * build_const_declaration(char * ident, struct et_node* sign, int value, struct sx_node * const_declaration_sub) {
    struct st_coordinates * coord = malloc(sizeof(struct st_coordinates));
    st_lookup(ident, st_const, coord, NULL);
    struct et_node * value_node = et_new_node(et_const, NULL, NULL, value, NULL);
    if(sign) {
      sign->left = value_node;
      value_node = sign;
    }
    struct sx_node* assign = sx_new_node(sx_assign, const_declaration_sub, NULL, NULL, coord, NULL, value_node, NULL);
    return assign;
  }

%}

%define parse.error verbose
%locations

%union {
  struct sx_node* syntax_node;
  struct ct_node* condition_node;
  struct et_node* expression_node;
  char string[MAX_KEY_LENGTH];
  int number;
}

%token
NULSYM
PLUSSYM
MINUSSYM
MULTSYM
SLASHSYM
ODDSYM
EQLSYM
NEQSYM
LESSYM
LEQSYM
GTRSYM
GEQSYM
LPARENTSYM
RPARENTSYM
COMMASYM
SEMICOLONSYM
PERIODSYM
BECOMESSYM
BEGINSYM
ENDSYM
IFSYM
THENSYM
WHILESYM
DOSYM
CALLSYM
CONSTSYM
INTSYM
PROCSYM
OUTSYM
INSYM
ELSESYM
DEBUGSYM
ERR;

%type<syntax_node>
  program
  block
  statement
  matched
  unmatched
  statement_begin
  const_declaration
  const_declaration_sub;
%type<condition_node>
  condition
  rel_op;
%type<expression_node>
  expression
  term
  expression_sign
  factor
  term_sub
  expression_sub;

%token<string>
  IDENTSYM;

%token<number>
  NUMBERSYM;

%%
program:
  block PERIODSYM
  {
    // finally creating the node for the main function.
    struct ft_node * main_function = ft_new_node($block, "main", 0, var_const_count);
    functions = ft_nodelist_next(functions, main_function);
    DEBUG_SYNTREE ? ft_print_tree(functions): NULL;
    // will translate the nodes to wat and write them to a file
    ft_to_wat(functions);
  }
  ;
block:
    { st_level_up(); }
  const_declaration var_declaration procedure_declaration statement
    {
      var_const_count = st_stackframe_size();
      st_level_down(); // symtab level down
      $$ = sx_nodelist_next($const_declaration, $statement);
    }
  ;
const_declaration:
  CONSTSYM IDENTSYM { if(!st_insert(yytext, st_const)) return -1; } EQLSYM expression_sign NUMBERSYM const_declaration_sub SEMICOLONSYM
    { $$ = build_const_declaration($IDENTSYM, $expression_sign, $NUMBERSYM, $const_declaration_sub); }
  | { $$ = NULL; }
  ;
const_declaration_sub:
  COMMASYM IDENTSYM { if(!st_insert(yytext, st_const)) return -1; } EQLSYM expression_sign NUMBERSYM const_declaration_sub
    { $$ = build_const_declaration($IDENTSYM, $expression_sign, $NUMBERSYM, $7); }
  | { $$ = NULL; }
  ;
var_declaration:
  INTSYM IDENTSYM  { if(!st_insert(yytext, st_var)) return -1; } var_declaration_sub SEMICOLONSYM
  |
  ;
var_declaration_sub:
  COMMASYM IDENTSYM  { if(!st_insert(yytext, st_var)) return -1; } var_declaration_sub
  |
  ;
procedure_declaration:
  PROCSYM IDENTSYM {
      if(!st_insert($IDENTSYM, st_proc)) return -1;
      struct ft_node * function = ft_new_node(NULL, $IDENTSYM, function_count++, 0);
      struct st_symtab_entry * entry;
      st_lookup($IDENTSYM, st_proc, NULL, &entry);
      entry->function_node = function;
      functions = ft_nodelist_next(function, functions);

    } SEMICOLONSYM block SEMICOLONSYM procedure_declaration
    {
      struct st_symtab_entry * entry;
      st_lookup($IDENTSYM, st_proc, NULL, &entry);
      entry->function_node->stackframe_size = var_const_count;
      entry->function_node->function = $block;
    }
  |
  ;
statement:
  matched       { $$ = $matched; }
  | unmatched   { $$ = $unmatched; }
  ;
matched:
  IDENTSYM BECOMESSYM expression
    {
      struct st_coordinates * coord = malloc(sizeof(struct st_coordinates));
      if(st_lookup($IDENTSYM, st_var, coord, NULL) < 1) return -1;
      $$ = sx_new_node(sx_assign, NULL, NULL, NULL, coord, NULL, $expression, NULL);
    }
  | CALLSYM IDENTSYM
    {
      struct st_symtab_entry * symtab_entry;
      struct st_coordinates * coord = malloc(sizeof(struct st_coordinates));
      if(st_lookup($IDENTSYM, st_proc, coord, &symtab_entry) < 1) return -1;
      struct sx_node * call = sx_new_node(sx_func_call, NULL, NULL, NULL, coord, NULL, NULL, symtab_entry->function_node);
      $$ = call;
    }
  | BEGINSYM statement statement_begin ENDSYM
    { $$ = sx_nodelist_next($statement, $statement_begin); }
  | IFSYM condition THENSYM matched ELSESYM matched
    { $$ = sx_new_node(sx_if_else, NULL, $4, $6, NULL, $condition, NULL, NULL); }
  | WHILESYM condition DOSYM matched
    { $$ = sx_new_node(sx_loop, NULL, $4, NULL, NULL, $condition, NULL, NULL); }
  | INSYM IDENTSYM
    {
      struct st_coordinates * coord = malloc(sizeof(struct st_coordinates));
      // check if variable exists and lookup coordinates
      if(st_lookup(yytext, st_var, coord, NULL) < 1) return -1;
      $$ = sx_new_node(sx_input, NULL, NULL, NULL, coord, NULL, NULL, NULL);
    }
  | OUTSYM expression
    { $$ = sx_new_node(sx_output, NULL, NULL, NULL, NULL, NULL, $expression, NULL); }
  | DEBUGSYM
    { $$ = sx_new_node(sx_debug, NULL, NULL, NULL, NULL, NULL, NULL, NULL); }
  | { $$ = NULL; }
  ;
unmatched:
  IFSYM condition THENSYM statement
    { $$ = sx_new_node(sx_if_else, NULL, $statement, NULL, NULL, $condition, NULL, NULL); }
  | IFSYM condition THENSYM matched ELSESYM unmatched
    { $$ = sx_new_node(sx_if_else, NULL, $4, $6, NULL, $condition, NULL, NULL); }
  | WHILESYM condition DOSYM unmatched
    { $$ = sx_new_node(sx_loop, NULL, $4, NULL, NULL, $condition, NULL, NULL); }
  ;
statement_begin:
  SEMICOLONSYM statement statement_begin
    { $$ = sx_nodelist_next($statement, $3); }
  |
    { $$ = NULL; }
  ;
condition:
  ODDSYM expression
    {
      $$ = ct_new_node(ct_odd);
      $$->left = $expression;
    }
  | expression rel_op expression
    {
      $rel_op->left = $1;
      $rel_op->right = $3;
      $$ = $rel_op;
    }
  ;
rel_op:
  EQLSYM
    { $$ = ct_new_node(ct_eq); }
  | NEQSYM
    { $$ = ct_new_node(ct_neq); }
  | LESSYM
    { $$ = ct_new_node(ct_lt); }
  | LEQSYM
    { $$ = ct_new_node(ct_lteq); }
  | GTRSYM
    { $$ = ct_new_node(ct_gt); }
  | GEQSYM
    { $$ = ct_new_node(ct_gteq); }
  ;
expression:
  expression_sign term expression_sub
    {
      struct et_node * node;
      if( $expression_sign ) {
        $expression_sign->left = $term;
        node = $expression_sign;
      } else node = $term;

      $$ = et_append_to_left_end($expression_sub, node);
    }
  ;

expression_sign:
  PLUSSYM       { $$ = NULL; }
  | MINUSSYM    { $$ = et_new_node(et_negate, NULL, NULL, 0, NULL); }
  |             { $$ = NULL; }
  ;

expression_sub:
  PLUSSYM term expression_sub
    {
      struct et_node * plus = et_new_node(et_add, NULL, $term, 0, NULL);
      $$ = et_append_to_left_end($3, plus);
    }
  | MINUSSYM term expression_sub
    {
      struct et_node * minus = et_new_node(et_subst, NULL, $term, 0, NULL);
      $$ = et_append_to_left_end($3, minus);
    }
  | { $$ = NULL; }
  ;
term:
  factor term_sub
    {
      $$ = et_append_to_left_end($term_sub, $factor);
    }
  ;
term_sub:
  MULTSYM factor term_sub
    {
      struct et_node * mult = et_new_node(et_mult, NULL, $factor, 0, NULL);
      $$ = et_append_to_left_end($3, mult);
    }
  | SLASHSYM factor term_sub
    {
      struct et_node * division = et_new_node(et_div, NULL, $factor, 0, NULL);
      $$ = et_append_to_left_end($3, division);
    }
  | { $$ = NULL; }
  ;
factor:
  IDENTSYM
    {
      // check if ident has already been declared
      struct st_coordinates * coord = malloc(sizeof(struct st_coordinates));
      if(st_lookup(yytext, st_var | st_const, coord, NULL) < 1) return -1; // exit parser with error code, if lookup fails
      $$ = et_new_node(et_var, NULL, NULL, 0, coord);
    }
  | NUMBERSYM                              { $$ = et_new_node(et_const, NULL, NULL, $NUMBERSYM, NULL); }
  | LPARENTSYM expression RPARENTSYM    { $$ = $expression; }
  ;
%%

